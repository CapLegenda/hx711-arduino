#include <HX711.h>                                              // подключаем библиотеку для работы с тензодатчиком

int t = 0;
int st = 0;                                       
                    
#define DT  A0                                                // Указываем номер вывода, к которому подключен вывод DT  датчика
#define SCK A1                                                // Указываем номер вывода, к которому подключен вывод SCK датчика
#define linear_k 0.83168                                      // Считаем, что напряжение и вес соотносятся линейно

HX711 scale;                                                  // создаём объект scale - отвечает за конкретный датчик(для двух датчиков scale1, scale2...)
s

                                                             
void setup() {

  Serial.begin(9600); 
  Serial.println(A0); 
  scale.begin(A1, A0);                        //В begin указываем выыводы к которым подключен датчик к ардуино
  scale.tare();                               //Берем текущие показания за 0
  scale.set_scale(600);                       // устанавливаем калибровочный коэффициент
}


void loop() {
  if (Serial.available()) {
    string s = Serial.readString();
    if (!strncmp(s, "/start", 6)) {
      st = 1;
    } else if (!strncmp(s, "/stop", 5)) {
      st = 0;
    }
  }
  
  if (st) { 
    mean_measure += scale.get_units(); //считываем значение 
    Serial.print(t); //Время считывания показания 
    Serial.print(";");
    t++;
    Serial.println(linear_k * mean_measure ); //домножаем на поправочный коэффициент полученный при измерениях 
  }                    
}
